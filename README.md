# **Javascript**


# **Table of Contents**

*  [Basic](#markdown-header-basic)
    *  [Varibale](#markdown-header-variable)
    *  [Constant](#markdown-header-constant)
    *  [Primitive Types](#markdown-header-primitive-types-value-types)
    *  [Dynamic Types](#markdown-header-dynamic-types)
    *  [Reference Types](#markdown-header-reference-types)
*  [Operators](#markdown-header-operators)
    *  [Arithmetic](#markdown-header-arithmetic)
    *  [Assignment](#markdown-header-assignment)
    *  [Comparison](#markdown-header-comparison)
    *  [Ternary](#markdown-header-ternary)
    *  [Logical](#markdown-header-logical)
    *  [Bitwise](#markdown-header-bitwise)
*  [Control Flow](#markdown-header-control-flow)
    *  [If-Else](#markdown-header-if-else)
    *  [Switch-Case](#markdown-header-switch-case)
    *  [For-Loop](#markdown-header-for-loop)
    *  [While-Loop](#markdown-header-while-loop)
    *  [Do-While-Loop](#markdown-header-do-while-loop)
    *  [For-in-Loop](#markdown-header-for-in-loop)
    *  [For-of-Loop](#markdown-header-for-of-loop)
    *  [Break-Continue](#markdown-header-break-continue)
*  [Objects](#markdown-header-objects_1)
    *  [basic](#markdown-header-basic_1)
    *  [Factory Functions](#markdown-header-factory-functions)
    *  [Constructor Functions](#markdown-header-constructor-functions)
    *  [Constructor Property](#markdown-header-constructor-property)
    *  [Dynamic Nature of Objects](#markdown-header-dynamic-nature-of-objects)
    *  [Functions are Objects](#markdown-header-functions-are-objects)
    *  [Value vs Reference Type](#markdown-header-value-vs-reference-type)
    *  [Enumerating Object Properties](#markdown-header-enumerating-object-properties)
    *  [Cloning an Object](#markdown-header-cloning-an-object)
    *  [Math Object](#markdown-header-math-object)
*  [Arrays](#markdown-header-arrays)
    *  [Add Element](#markdown-header-add-element)
    *  [Find Element](#markdown-header-find-element)
    *  [Arrow Function](#markdown-header-arrow-function)
    *  [Remove Element](#markdown-header-remove-element)
    *  [Empty Array](#markdown-header-empty-array)
    *  [Combining & Slicing Array](#markdown-header-combining-slicing-array)
    *  [Spread Operator](#markdown-header-spread-operator)
    *  [Iterating Array](#markdown-header-iterating-Array)
    *  [Join & Split Array](#markdown-header-join-split-array)
    *  [Sorting Array](#markdown-header-sorting-array)
    *  [Testing Array Elements](#markdown-header-testing-array-elements)
    *  [Filtering, Mapping Array & Chaining Functions](#markdown-header-filtering-mapping-array-chaining-functions)
    *  [Reducing Array](#markdown-header-reducing-array)
    *  [Array Functions Revised](#markdown-header-array-functions-revised)
*  [Functions](#markdown-header-functions)
    *  [Declaration vs Expression](#markdown-header-declration-vs-expression)
    *  [Arguments](#markdown-header-arguments)
    *  [Rest Operator](#markdown-header-rest-operator)
    *  [Default Parameters](#markdown-header-default-parameters)
    *  [Getters & Setters](#markdown-header-getters-setters)
    *  [Try & Catch](#markdown-header-try-catch)
    *  [This Keyword](#markdown-header-this-keyword)
*  [Concepts](#markdown-header-concepts)
    *  [Hoisting](#markdown-header-hoisting)
    *  [Destructuring](#markdown-header-destructuring)
    *  [Generators](https://codeburst.io/understanding-generators-in-es6-javascript-with-examples-6728834016d5)


    



## Basic

* #### **Variable**

Use ‘let’ eyword to declare and assign variable.

```javascript
let name = ‘usman’;
```
Variable name can not be a reserved word
Variable name can not be start with 0 and contains space or hyphen (-)

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Constant**

Use ‘const’ keyword to declare and assign a variable if its value can’t be changed in future.

```javascript
const interestRate = 0.3;
interestRate = 1; //error
```
Can't reassign the value of const

* #### **Primitive Types / Value Types**

1- String

```javascript
let name = ‘usman’;  //type is string
```

2 - Number

```javascript
let age = 30;  //type is number
```

3- Boolean

```javascript
let isApproved = false;  //type is boolean
```

4- Undefined

```javascript
let firstName = undefined;  //type is undefined
Or 
let firstName;  //type is undefined
```

5- Null

```javascript
let selectedColor = null;  //type is object
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Dynamic Types**

We can change the type of a variable at runtime.

```javascript
let name = ‘usman’;  //type is string
name = 1;  //type is changed from string to number
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Reference Types**

There are 3 reference types:

1.  Objects
2.  Arrays
3.  Functions

### Objects

Use object literals '{}' to declare an object.

```javascript
//object declared
let person = {
   name: 'Usman',
   age: 30
}

//access object property and changed value
//with dot notation
person.name = 'John';

//access object property and changed value
//with bracket notation
person['name'] = 'John';

or

let selection = 'name';
person[selection] = 'Mary';
```

**[⬆ back to top](#markdown-header-table-of-contents)**

### Arrays

Use array notation '[]' to declare array.

```javascript
//array declared
let selectionColors = ['red', 'blue']; //type of array is object

//access element value
console.log(selectionColors[0]);
//output => 'red'

//access element value and changed it
selectionColors[0] = 'green';
console.log(selectionColors[0]);
//output => 'green'

//add new value
selectionColors[2] = 'yellow'
selectionColors[3] = 4; //we can have different types of value in same array
console.log(selectionColors);
//output => ['green', 'blue', 'yellow', 4]
```

**[⬆ back to top](#markdown-header-table-of-contents)**

### Functions

Use 'function' keyword to declare it.

```javascript

//declared a function with no parameter
function greet() {
   console.log('Hello World');
   }
   
//calling a function
greet()
//output => Hello World

//declared a function with 1 parameter
function greet(name) {
   console.log('Hello ' + name);
   }

//calling a function
greet('Usman');
//output => Hello Usman

//declared a function with 2 parameters
function greet(name, lastName) {
   console.log('Hello ' + name + ' ' + lastName);
   }

//calling a function
greet('Usman');
//output => Hello Usman Undefined

greet('Usman', 'Iqbal');
//output => Hello Usman Iqbal

//declared a function with return keyword
function square(number) {
      return number * number;
      }
```

**[⬆ back to top](#markdown-header-table-of-contents)**

## Operators

* #### **Arithmetic**

```javascript
let x = 10;
let y = 3;

console.log(x + y); //addition
//output => 13
console.log(x - y); //subraction
//output => 7
console.log(x * y); //multiplication
//output => 30
console.log(x / y); //drivision
//output => 3.33
console.log(x % y); //modulous
//output => 1
console.log(x ** y); //x power y
//output => 1000

//Increment
console.log(x++) //post increment
//output => 10
console.log(++x) //pre increment
//output => 11

//Decrement
console.log(x--) //post decrement
//output => 10
console.log(--x) //pre decrement
//output => 9
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Assignment**

Use '=' to assign values.

```javascript
let x = 10;
```

Assignment operators can aslo be used with arithmetic operators.

```javascript
let x = 10;
x = x + 5;
//can be write as
x += 5;

x = x * 3;
//can be write as
x *= 3;
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Comparison**

1.  Objects evaluate to true
2.  Undefined evaluates to false
3.  Null evaluates to false
4.  Booleans evaluate to the value of the boolean
5.  Numbers evaluate to false if +0, -0, or NaN, otherwise true
6.  Strings evaluate to false if an empty string '', otherwise true

```javascript
let x = 1;

//Relational
console.log(x > 0);
//output => ture
console.log(x >= 0);
//output => ture
console.log(x < 1);
//output => false
console.log(x <=1 );
//output => ture
console.log(3>2>1); // 3>2 evaluate to true, 1 is cnverted to true, then true > true which results into false
//output => false
console.log(3>2>0); // 3>2 evaluate to true, 0 is cnverted to false, then true > false which results into true
//output => true

//Equality
console.log(x === 1);
console.log(x !== 1);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Ternary**

```javascript
let points = 110;
let type = points > 100? 'gold' : 'silver';
console.log(type);
//output => 'gold';
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Logical**

There are 3 logical operators.
1.  AND (&&)
2.  OR (||)
3.  Nor (!)

```javascript
//Example 1
let highIncome = false;
let goodCreditScore = false;
let eligibleForLoan = highIncome && goodCreditScore;
console.log(eligibleForLoan);
//output => false

let applicationRefused = !eligibleForLoan;
console.log('Applcation Refused ', applicationRefused);
//output => true

//Example 2
let highIncome = true;
let goodCreditScore = false;
let eligibleForLoan = highIncome || goodCreditScore;
console.log(eligibleForLoan);
//output => true

let applicationRefused = !eligibleForLoan;
console.log('Applcation Refused ', applicationRefused);
//output => false
```

Falsy Values

*  undefined

*  null

*  0

*  false

*  ''

*  NaN

All values other than falsy are truthy

**How it is operating?**

If first operand is truthy and next operator is 'OR' then it will not further check any operand and print the first operand.

If first operand is falsy and next operator is 'OR' then it will check next operand,if its also falsy, then repeat the previous process until find truthy. If there is no truthy, it wll print last falsy value found. if there is truthy it will print the first truthy value found.


If first operand is falsy and next operator is 'AND' then it will not further check any operand and print the first operand.

If first operand is truthy and next operator is 'AND' then it will check next operand, if its also truthy, then repeat the previous process until find falsy. If there is no falsy, it wll print last truty value found. if there is falsy it will print the first falsy value found.

```javascript
console.log(false || 'Mosh')
//output => Mosh
console.log(true || 'Mosh')
//output => true
console.log('Mosh' || false)
//output => Mosh
console.log('Mosh' || true)
//output => Mosh
console.log(false && 'Mosh')
//output => false
console.log(true && 'Mosh')
//output => Mosh
console.log('Mosh' && false)
//output => false
console.log('Mosh' && true)
//output => true
console.log(true || 'mosh' && false)
//output => true
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Bitwise**

It converst the decimal into 8 bits of number and compare each bit separately.

```javascript
console.log(1 | 2);
//output => 3

//solution
// 1  = 00000001
// 2  = 00000010
// OR = 00000011 = 3

console.log(1 & 2);
//output => 0

//solution
// 1   = 00000001
// 2   = 00000010
// AND = 00000000 = 0
```

**[⬆ back to top](#markdown-header-table-of-contents)**

## Control Flow

* #### **If-Else**


```javascript
let hour = 20;

if (hour >= 6 && hour < 12) {
   console.log('Good Morning');
} else if (hour >= 12 && hour < 18) {
   console.log('Good Afternoon');
} else {
   console.log('Good Evening');
}

//output => 'Good Evening'
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Switch-Case**

```javascript
let role = 'guest';

switch (role) {
   case 'guest':
      console.log('Guest User');
      break;
   case 'moderator':
      console.log('Moderator User');
      break;
   default:
      console.log('Unknown User');
}

//output => 'Guest User'
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **For-Loop**

```javascript
for (let i = 0; i < 5; i++) {
   console.log('Hello World');
}

//output => Hello World
//output => Hello World
//output => Hello World
//output => Hello World
//output => Hello World

for (let i = 1; i <= 5; i++) {
   console.log('Hello World ', i);
}

//output => Hello World 1
//output => Hello World 2
//output => Hello World 3
//output => Hello World 4
//output => Hello World 5

for (let i = 1; i <= 5; i++) {
   if (i % 2 !== 0) console.log(i);
}

//output => 1
//output => 3
//output => 5

for (let i = 5; i >= 1; i--) {
   if (i % 2 !== 0) console.log(i);
}

//output => 5
//output => 3
//output => 1
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **While-Loop**

```javascript
let i = 0;
while (i <= 5) {
   if (i % 2 !== 0) console.log(i);
   i++;
}

//output => 1
//output => 3
//output => 5
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Do-While-Loop**

```javascript
let i = 0;
do {
   if (i % 2 !== 0) console.log(i);
   i++;
} while (i <= 5);

//output => 1
//output => 3
//output => 5
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **For-in-Loop**

Used to iterate over objects properties and arrays element.

```javascript
const person = {
   name: 'Usman',
   age: 30
}

for (let key in person) {
   console.log(key, person[key]);
}

//output => name Usman
//output => age 30

//Not recommend to iterate array with for-in-loop
const colors = ['red', 'green', 'blue'];

for (let index in colors) {
   console.log(index, colors[index]);
}

//output => 0 red
//output => 1 green
//output => 2 blue
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **For-of-Loop**

Used to iterate over arrays element

```javascript
const colors = ['red', 'green', 'blue'];

for (let color of colors) {
   console.log(color);
}

//output => red
//output => green
//output => blue
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Break-Continue**

```javascript
let i = 0;

while (i <= 10) {
   if (i === 5) {
      break;
   }
   console.log(i);
   i++;
}

//output => 0
//output => 1
//output => 2
//output => 3
//output => 4

let j = 0;

while (j <= 10) {
   if (j % 2 === 0) {
      continue;
   }
   console.log(j);
   j++;
}

//output => 1
//output => 3
//output => 5
//output => 7
//output => 9
```
**[⬆ back to top](#markdown-header-table-of-contents)**

## Objects

Objects can be defined with object literals i.e '{}'. 

Objects can have key value pairs. Values can be of primitive types or reference types.


* #### **Basic**

```javascript
const circle = {
    radius: 1,
    location: {
        X: 1,
        Y: 1
    },
    isVisible: true,
    draw: function () {
        console.log('draw');
    }
}

//initialize circle
circle.draw(); //Method


//output => 'draw'

```

**Note: If function is declared in an object, we should prefer to call it method not fucntion.**

If we want to make a new circle we have to repeat our code.  So instead of repeating our circle code we can change the implementation.

We can modify the implementation in either way:

1.	Factory Functions

2.	Constructor Functions

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Factory Functions**
In factory functions, we just define the object implementation in a function and return that object. And we initialize the object by calling the function.

```javascript
//If key and value name is same, we just write it once as below
function createCircle(radius) {
    return {
        radius,
        draw() {
            console.log('draw');
        }
    }
}

const circle1 = createCircle(1);
console.log(circle1);

//output => {
//   radius: 1,
//   f draw()
//   }

const circle2 = createCircle(2);
console.log(circle2);

//output => {
//   radius: 2,
//   f draw()
//   }

```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Constructor Functions**

In constructor functions, we just define the object implementation in a function named with pascal notation and use ‘this’ keyword to assign values to object. And use ‘new’ keyword to initialize the object.

```javascript
//here ‘this’ refers to the current object, the object in the current context.
function Circle(radius) {
    this.radius = radius;
    this.draw = function () {
        console.log('draw');
    }
}

const circle = new Circle(1);

//output => {
//   radius: 1,
//   f draw()
//   }
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Constructor Property**

Every object in javascript has a constructor property. This constructor property refers to the function that construct / create that object.

```javascript
function Circle(radius) {
    this.radius = radius;
    this.draw = function () {
        console.log('draw');
    }
}

const another = new Circle(1);
console.log(another.constructor)

//output
// f Circle(radius) {
//	same implementation as above
//	}


function createCircle(radius) {
    return {
        radius,
        draw() {
            console.log('draw');
        }
    }
}

const circle = createCircle(1);
console.log(circle.constructor)

//output
// f Object() {
//	[native code]
//	}

```

As we know creating object with ‘new’ keyword is actually a constructor function approach that’s why first example output is the same function it self.

But in second example we are not creating object with constructor approach. That’s why javascript handle our code at a nutshell and convert the following code:

const circle = createCircle(1);

into this

const circle = new Object(1); //this a constructor function

**Note: when we create object with constructor approach, then 'initialization.constructor' is the same function.
But when we create object with literal / factory function approach, then javascript compiler 
will create  constructor function named Object to create our object.
At the end both constructor functions, i.e our own defined / javascript defined (Object) 
will call the following function to create object.**

*f Function() { [native code] }*

Other built in constructors in javascript are

new String()

new Boolean()

new Number()

When we initialize string, boolean and number with literals, they actually converted into the above constructors to be created.

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Dynamic Nature of Objects**

We can modify the object after its declaration and initialization by adding new properties or deleting existing properties.

```javascript
const circle = {
    radius: 1
}

circle.color = 'yellow';
circle.draw = function () { }

delete circle.color;

console.log(circle);
 
//output => {
//   radius: 1,
//   f draw()
//   }

```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Functions are Objects**

```javascript
function Circle(radius) {
    this.radius = radius;
    this.draw = function () {
        console.log('draw');
    }
}

const another = new Circle(1);
//OR
Circle.call = ({}, 1)

//both calls will have same output

//output => {
//   radius: 1,
//   f draw()
//   }

// Above both codes are same, actually when we call with ‘new’ keyword, 
// it converted it into the ‘.call’ function. And ‘this’ keyword refers to that empty object in ‘.call’ function.

// If we have multiple parameter we can call the function as follow

Circle.apply({}, [1, 2, 3]);

```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Value vs Reference Type**

Assigning Value vs Reference type into another variable

```javascript

//example 1

let x = 10;
let y = x;

x = 20;

console.log(x)

//output => 20

console.log(y)

//output => 10

//Here just value of x copy into y, and y has its own separate identity thats why change in x does not
//reflect in y

//example 2

let x = { value: 10};
let y = x;

x.value = 20;

console.log(x)

//output => { value: 20}

console.log(y)

//output => { value: 20}

//Here x is an object. object variable holds the memory address where actual value is stored,
//so when we copy cx in y, the same memory address copied and y also points to the same memory
//when we changed value of x, value in memory changes y is also pointing to same memory thats why
//value of y is also changed

```

Passing Value vs Reference type into a fucntion. i.e, pass by value and pass by reference respectively.

```javascript

//example 1

let number = 10;

function increase(number) {
   number++;
}

increase(number);

console.log(number)

//output => 10

//When we passed number, its value is passed and parameter 'number' in increase function stored 
//its value with a separate identity which has scoped only in function

//expamle 2

let obj = { value: 10};

function increase(obj) {
   obj.value++;
}

increase(obj);

console.log(obj)

//output => {
//   value: 11
//}

//Here when we passed obj, its reference in memory is passed and parameter 'obj' in increase funtion
//stored its reference. When we call increase function, the increment happens to the value in memory
//thats why when we console the obj its value is ncremented.
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Enumerating Object Properties**

```javascript
function circle() {
    return {
        radius: 1,
        draw() {
            console.log('draw');
        }
    }
}

for (let key in circle)
   console.log(key, circle[key]);

//output => radius 1
//output => draw f

for (let key of circle)
   console.log(key);
   
 //output => Error
 //for-of is only used for iterable objects and iterable objects are arrays and maps.
```

Not recommended but we can still use for-of to iterate over object as follow:

```javascript
function circle() {
    return {
        radius: 1,
        draw() {
            console.log('draw');
        }
    }
}

//Object.keys(circle), will return the keys in an array

for (let key of Object.keys(circle))
   console.log(key);

//output => radius
//output => draw

//Object.enteries(circle), will return each property in a separate array as key at index 0
//and value at index 1

for (let entry of Object.enteries(circle))
   console.log(entry);
   
//output => ['radius',1]
//output => ['draw',f]

//to check if key exists in an object or not

if ('radius' in circle) console.log('yes')

//output => yes
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Cloning an Object**

```javascript
const circle = {
    radius: 1,
    draw: function () {
        console.log('draw');
    }
}

//not recommended
const another = {};
for (let key in circle)
   another[key] = circle[key]

//middle approach
const another1 = Object.assign({}, circle);

//you can add new properties during cloning as follow
const another2 = Object.assign({color: 'yellow'}, circle);

//best and modern approach => spread operator
const another3 = { ...circle };

//you can add new properties during cloning as follow
const another3 = { color: 'yellow', ...circle };
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Math Object**

```javascript
console.log(Math.random())

//output => generates random number between 0 & 1

console.log(Math.round(1.9))

//output => 2, roundoff the number

console.log(Math.max([1,2,3,4,5])

//output => 5, fnds maximum number

console.log(Math.min([1,2,3,4,5])

//output => 1, fnds minimum number
```

**[⬆ back to top](#markdown-header-table-of-contents)**

## Arrays

* #### **Add Element**

```javascript
const numbers = [3,4]

//add at end
numbers.push(5,6)
console.log(numbers)

//output => [3,4,5,6]

//add at start
numbers.unshift(1,2)
console.log(numbers)

//output => [1,2,3,4,5,6]

//add at middle / any position
numbers.splice(2,0,'a','b'); //Array.splice(start index, no of elements to be removed, new elements to be add = optional)
console.log(numbers)

//output => [1,2,3,'a','b',4,5,6]
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Find Element**

Primitive Values
    
```javascript
const numbers = [1,2,3,4]
console.log(numbers.indexOf('a')); //if element does not exist it will return -1.

//output => -1

console.log(numbers.indexOf(1)); //if element exists it will return the index.
    
//output => 0

//return the index of last occurence element
const number = [1,2,3,1,4]
console.log(number.lastIndexOf(1)); //if element exists it will return the last occurence index otherwise -1.

//output => 3

//verify element exist in array or not
//this method is not recommended to use
console.log(number.indexOf(1) !== -1) //if exists return true, otherwise false

//output => true

//verify element exist in array or not
//this method is recommended to use
console.log(number.includes(1)) //if exists return true, otherwise false

//output => true

//All the above methods, indexOf(), lastIndexOf() and includes() have 2nd parameter
//which is optional. This parameter species the starting point.
console.log(number.indexOf(1,2))

//output => 3
//second parameter '2', specfies to apply the indexOf method on array from index 2.
//thats why index 0 & 1 are ignored.
```

Reference Values

```javascript
const courses = [
{id: 1, name: 'a'},
{id: 2, name: 'b'},
{id: 3, name: 'a'}
];

const course = courses.find(function (course) {
return course.name === 'a';
});

console.log(course);

//output => {id: 1, name: 'a'}
//if the cousrse does not exist, it will return undefined
//if course exist, it will only return first matched object

const courseIndex = courses.findIndex(function (course) {
return course.name === 'a';
});

console.log(courseIndex);

//output => 0
//if the cousrse does not exist, it will return -1
//if course exist, it will only return first matched object index
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Arrow Function**

```javascript
const courses = [
{id: 1, name: 'a'},
{id: 2, name: 'b'},
{id: 3, name: 'a'}
];

//steps
//remove function keyword
//add round brackets around parameters if more than 1 parameter, otherwise dont put round bracket
//if brackets, put arrow sign after it, if no bracket, put arrow sign after 1 parameter
//use curly brackets if defination is more than 1 line, otherwise dont use curly brakets, just put that one line code infront of arrow sign
//dont use return keyword if there is oneline putting infront of arrow sign

const course = courses.find(course => course.name === 'a');

console.log(course);

//output => {id: 1, name: 'a'}
//if the cousrse does not exist, it will return undefined
//if course exist, it will only return first matched object
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Remove Element**

```javascript
const numbers = [1,2,3,4,5,6]

//remove at end
numbers.pop()
console.log(numbers)

//output => [1,2,3,4,5]

//remove at start
numbers.shift()
console.log(numbers)

//output => [2,3,4,5]

//remove at middle / any position
numbers.splice(1,2); //Array.splice(start index, no of elements to be removed, new elements to be add = optional)
console.log(numbers)

//output => [2,5]
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Empty Array**

```javascript
let numbers = [1,2,3,4];
let another = numbers;

//sol 1 recommended if not having any reference
numbers = [];
//As another is refrencing to numbers, but when we empty numbers array, it started pointing
//to new location and hence the another array still have a copy of numbers array

//sol2 recommended if having any reference
numbers.length = 0;
//this approach will empty the numbers array and also empty all the refrences of numbers array

//sol3 not recommended
numbers.splice(0, numbers.length);

//sol4 not recommended
while(numbers.length > 0) {
    numbers.pop()
}
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Combining & Slicing Array**

Primitive Values

```javascript
const first = [1,2,3];
cosnt second = [4,5,6];

const combined = first.concat(second)

const slice = combined.slice(2,4) //Array.slice(start index = optional, Index before end = optional)
//here slice wil start from index 2 and stop before index 4

console.log(combined);

//output => [1,2,3,4,5,6]

console.log(slice);

//output => [3,4]

//if we send only 1 param
const slice1 = combined.slice(2)
//start from index 2 till last element

//ignore slice operation
//output => [3,4,5,6]

//if we send only 0 param
const slice2 = combined.slice()
//start from index 0 till last element, means copy the whole array

//ignore slice & slice1 operation
//output => [1,2,3,4,5,6]

```

Reference Values

```javascript
cosnt first = [{id: 1}];
const second = [4,5,6];

const combined = first.concat(second)

first[0].id = 10;

const slice = combined();

console.log(combined)

//output => [{id: 10},4,5,6]

console.log(slice)

//output => [{id: 10},4,5,6]
```

We have changed the id after concat method still we get id value 10 in combined array. Thats because it is a reference type, and combined array was having a reference object. When we changed the id after concat method, change also reflect in combined array.

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Spread Operator**

it is used to copy the elements.

```javascript
const first = [1,2,3];
cosnt second = [4,5,6];

const combined = [...first, 'a', ...second, 'b'];
const copy = combined;
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Iterating Array**

```javascript
const numbers = [1,2,3];

for (let number of numbers)
    console.log(number)

//preffered approach, index parameter is optional
numbers.forEach((number, index) => console.log(index, number));
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Join & Split Array**

join Arrays element in a single string

```javascript
const numbers = [1,2,3];
const joined = numbers.join(',');
console.log(joined

//output => 1,2,3
```

split string into array elements

```javascript
const message = 'This is my first message';
const parts = message.split(' ');
console.log(parts)

//output => ['this','is','my','first','message']
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Sorting Array**

Primitive Values

```javascript
const numbers = [2,3,1];
numbers.sort(); //sort in ascending order
console.log(numbers);

//output => [1,2,3]

numbers.reverse(); //sort in descending by reversing array
console.log(numbers);

//output => [3,2,1]
```

Reference Values

```javascript
const courses = [
    {id: 1, name: 'Node js'},
    {id: 2, name: 'Javascript'}
];

courses.sort(function(a,b) {
    const nameA = a.name.a.toLowerCase();
    const nameB = a.name.b.toLowerCase();
    
    if (nameA < nameB) return -1;
    if (nameA > nameB) return 1;
    return 0;
})
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Testing Array Elements**

```javascript
const numbers = [1,2,3];

//condition must fulfill on all elements
const allPositive = numbers.every(function(value) {
    return value >= 0;
})

//condition must fulfill on atleast one element
const allPositive = numbers.some(function(value) {
    return value >= 0;
})
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Filtering, Mapping Array & Chaining Functions**

```javascript
const numbers = [1,2,-1,3];

const filtered = numbers
    .filter(value => value >= 0) //output => [1,2,3]
    .map(n => ({value: n}) //output [{value: 1}, {value: 2}, {value: 3}]

```

**Note: In above code there is a object literal after arrow function in map. Thats why we put round brackets to make sure it does not preceive it as function brackets.**

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Reducing Array**

It reduces the array elements into single value.
```javascript
const numbers = [1,2,-1,3];

const sum = numbers.reduce(
    (accumulator, currentValue) => accumulator + currentValue
)
console.log(sum)

//output => 5

//if want to set accumulator value
const sum1 = numbers.reduce(
    (accumulator, currentValue) => {
        return accumulator + currentValue
    }, 10
)

console.log(sum)

//output => 15

```

If we dont set value of accumulator, by default it would be the first element of array.
Current Value starts from the array first element if we have set accumulator and starts from the second element If we have not et accumulator.

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Array Functions Revised**

1. push()
2. unshift()
3. splice()
4. indexOf()
5. lastIndexOf()
6. includes()
7. find => callback function
8. findIndex => callback function
9. pop()
10. shift()
11. concat()
12. slice()
13. forEach => callback function
14. join()
15. split()
16. sort()
17. reverse()
18. every()
19. some()
20. filter()
21. map
22. reduce

**[⬆ back to top](#markdown-header-table-of-contents)**

## Functions

* #### **Declaration vs Expressions**

```javascript
//Function Declaration
function walk() {
    console.log('walk');
}

//Anonymous Function Expression
let run - function() {
    console.log('run');
}

let move = run;

run();
move();
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Arguments**

```javascript
function sum(a,b) {
    return a + b;
}
console.log(sum(1,3));

//output => 4
```
If I pass only 1 argument or even 0 arguments. we will get NaN error because in this case it will take the default values of arguments which is undefined.

If I pass more than 2 arguments, it will not throw any error, it will just take first 2 parameters that function require and ignore the rest.

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Rest Operator**

If we dont know no of arguments pass to the function, we can use rest operator for this.

```javascript
//Example 1
function sum(...args) {
    return args.reduce((a,b) => a + b);
}
console.log(sum(1,2,3,4,5,10));

//output => 25

//Example 2
//rest operator parameter must be written as last parameter.
function sum(discount,...prices) {
    let total = prices .reduce((a,b) => a + b);
    return total * (1 - discount);
}
//Here 1 arguments will be passed to the discount and the rest will be passed to the prices.
console.log(sum(0.1,20,30));

//output => 45
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Default Parameters**

```javascript
fucntion interest(principal, rate = 3.5, years = 3) {
    return principal * rate / 100 * years;
}
console.log(10000, 4);
```

We can define the default values in function parameters, if we dont pass value for that parameter, it will use the default one.

**Note: Parameters with default values, must be write as last / right most parameters

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Getters & Setters**

```javascript
const person = {
    firstName: 'Usman',
    lastName: 'Iqbal',
    get fullName() {
        return `${person.fullName} ${person.lastName}`
    },
    
    set fullName(value) {
        const parts = value.split(' ');
        this.firstName = parts[0];
        this.lastName = parts[1];
    }
}

person.fullName = 'John Smith';
console.log(person);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Try & Catch**

```javascript
const person = {
    firstName: 'Usman',
    lastName: 'Iqbal',
    get fullName() {
        return `${person.fullName} ${person.lastName}`
    },
    
    set fullName(value) {
        if (typeof value !== 'string')
            throw new Error('Value is not a string.');
            
        const parts = value.split(' ');
        if (parts.length !== 2) 
            throw new Error('Enter a first and last name');
            
        this.firstName = parts[0];
        this.lastName = parts[1];
    }
}

try {
    person.fullName = 'John Smith';
} catch (e) {
    alert(e);
}

console.log(person);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **This Keyword**

```javacsript
//method -> obj
//if function as method is defined in an object then 'this' refers to the current object.

//function -> global (window, global)
//if function is defined as global or outside the object then 'this' refers to window or global object

const video = {
    title: 'a',
    play() {
        console.log(this);
    }
}

video.stop = function() {
    console.log(this)
}

video.play();

//output => it will print the defination of video object because play method is defined in a video object.

video.stop();

//output => it will print the defination of video object because stop method is defined in a video object using object reference.

function playVideo() {
    console.log(this);
}

playVideo();

//output => it will print the window object because function is global or a regular fucntion.

//constructor function
function Video(title) {
    this.title = title
    console.log(this);
}

const v = new Video('b');

//output => it will print the Video object instead of window object.
//why? because when we use new operator, at the nutt shell it first creates the new empty object i.e '{}'.
//And because of this empty object 'this' refers to Video(current) object instead of window object.
```

Callback function in an object.

```javascript
const video = {
    title: 'a',
    tags: ['a','b','c'],
    showTags() {
        this.tags.forEach(function(tag) {
            console.log(this.title, tag);
        })
    }
}

//output => undefined 'a'
//output => undefined 'b'
//output => undefined 'c'

//Here show tags is the part of video object, thats why 'this' refers to video object and we easily access the tags array
//But we are accessing title in a callback function, and callback function is just a normal regular funtion,
//so in callback function, 'this' refers to global object.
//we can handle this in various ways

//way 1
//we have an optional second argument in forEach loop to send object. So we can send a custom object
//or a reference of any object in second argument

const video = {
    title: 'a',
    tags: ['a','b','c'],
    showTags() {
        this.tags.forEach(function(tag) {
            console.log(this.title, tag);
        }, this)
    }
}

//output => 'a' 'a'
//output => 'a' 'b'
//output => 'a' 'c'

//Here showTags is a part of video obj, so when we pass 'this' in second argument,
//it actually referencing to the video object instead of global object.
//But no all methods provide second argument to send object, so this approach is very limited

//way 2 Not Recommended Approach
//Imaging forEach does not have second argument to pass object.

const video = {
    title: 'a',
    tags: ['a','b','c'],
    showTags() {
        //At this moment 'this' still refers to video object;
        const self = this;
        this.tags.forEach(function(tag) {
            //Here reference of 'this' changes to global, but we already had the refernce of
            //video object in self, as we copied the reference before
            console.log(self.title, tag);
        })
    }
}

//way 3
//As functions are objects, so function has some properties and methods
//we will use call, bind, apply method to change the 'this' reference from window to current object.

//Call method
fucntion playVideo(a,b) {
    console.log(this)
}

playVideo.call({name: 'Usman'},1,2)

//output => {name: 'Usman'}

playVideo.apply({name: 'Usman'}.[1,2]) //The only difference b/w 'call' and 'apply' is that, In 'apply' we have to pass arguments as an array.

//output => {name: 'Usman'}

playVideo.bind({name: 'Usman'})(); //'bind' actually returns the passed object, then we can store the result in a variable and call the function, also we can immediate call the function.

//output => {name: 'Usman'}

playVideo();

//output => window object

//Even function is declared as regular function, but when we call it with 'call' method, it refers to current object
//And when call it in a simple way, it refers to window object.

const video = {
    title: 'a',
    tags: ['a','b','c'],
    showTags() {
        //At this moment 'this' still refers to video object;
        const self = this;
        this.tags.forEach(function(tag) {
            console.log(this.title, tag);
        }.bind(this))
    }
}

```

**[⬆ back to top](#markdown-header-table-of-contents)**

## Concepts

* #### **Hoisting**

Hoisting means to move their declaration or assignment at the top of the closest fucntion scope.

we can only call anything, after it is declared.

**Rules**

1. If we declare and assign 'var' variable, after calling, it will hoist its declaration before calling but not assignment.
2. If we declare and assign 'let' / 'const' variable, after calling, it will not hoist its declaration before calling niether assignment.
3. If we create a function with declaration, and call it before declation, then function will hoist its declaration and assignment.
4. If we create a function with expression, and call it before declaration, then result will depend upon the expression. Whether its declared with 'var', 'let' or 'const'.

```javascript
walk()

fucntion walk() {
    console.log(walk);
}

console.log(run)
run()

//output => reference error
//output => reference error

const run = function() {
    console.log('run');
}

console.log(run1)
run1()

//output => reference error
//output => reference error

let run1 = function() {
    console.log('run');
}

console.log(run2)
run1()

//output => undefined
//output => reference error

var run2 = function() {
    console.log('run');
}

//All above examples of anonymous functions, behave same if we have function name, doesnot matter if function name and variable name is same or not

console.log(name)

//output => reference error

let name = 'usman';

console.log(name1)

//output => reference error

const name1 = 'usman';

console.log(name2)

//output => undefined

var name2 = 'usman';
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Destructuring**

When we want to get the value from a structure i.e object or array. we simply use 'dot' notation or 'square' bracket notation for objects and index number for array. This approach is fine but old so here we can use destructuring approach to get these values efficiently.

Object Destructuring

```javascript
// good
function getFullName(user) {
  const { firstName, lastName } = user; //Instead of user.firstName & user.lastName
  return `${firstName} ${lastName}`;
}

// best
function getFullName({ firstName, lastName }) {
  return `${firstName} ${lastName}`;
}
```

Array Destructuring

```javascript
const arr = [1, 2, 3, 4];

// good
const [first, second] = arr; //Instead of arr[0] & arr[1]

//Array can only be destruture in the sequence as it is in original.
//If you want to skip any index value, use '_' for it. e.g

const [first, _ , third] = arr; //Instead of arr[0] & arr[2]
```

Return Multiple Values

Use object destructuring for multiple return values instead of array destructuring.

```javascript
// bad
function processInput(input) {
  // then a miracle occurs
  return [left, right, top, bottom];
}

// the caller needs to think about the order of return data
const [left, __, top] = processInput(input);

// good
function processInput(input) {
  // then a miracle occurs
  return { left, right, top, bottom };
}

// the caller selects only the data they need
const { left, top } = processInput(input);
```